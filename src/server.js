const http = require('http');
const app = require('./core/express');
const config = require('./config/env-vars');
const logger = require('./config/logger');

// Create Http Server
const server = http.createServer(app);

server.listen(config.PORT);

server.on('listening', () => {
  logger.error(`${config.NODE_ENV.toUpperCase()} Server is Listening on PORT ${config.PORT}`);
});

// Listen to error on listening to port
const onError = (error) => {
  if (error.syscall !== 'listen') {
    throw error;
  }
  const bind = typeof port === 'string' ? `Pipe ${config.PORT}` : `Port ${config.PORT}`;
  switch (error.code) {
    case 'EACCES':
      logger.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      logger.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
};
server.on('error', onError);
// server.on ('/api',require('../src/api/routes'))
/**
 * Exports Express
 * @public
 */
module.exports = server;

const successResponse = (res, data, code = 200) => res.status(code).send({
  code,
  data,
  success: true,
});

const errorResponse = (res, message, serverCode=200,code=500) => res.status(serverCode).send({
  code,
  message,
  success: false,
});

module.exports = {
  successResponse,
  errorResponse
};

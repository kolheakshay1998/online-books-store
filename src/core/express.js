const express = require('express');
const helmet = require('helmet');
const compress = require('compression');
const cors = require('cors');
const router = require('../api/routes');

/**
 * Instantiate Express Framwork
 * @public
 */
const app = express();

// Use Json
app.use(express.json());
// Gzip Compression
app.use(compress());

// Secure apps by setting various HTTP headers
app.use(helmet());

// Enable CORS - Cross Origin Resource Sharing
app.use(cors({ maxAge: 7200 }));

// Route registration
app.use('/api', router);

module.exports = app;

class APIError extends Error {
  constructor({
    message,
    stack,
    errors = [],
    status = 500,
    isPublic = false,
  }) {
    super(message);
    this.name = this.constructor.name;
    this.message = message;
    this.errors = errors;
    this.status = status;
    this.isPublic = isPublic;
    this.stack = stack;
  }
}

const logger = require('../config/logger')
logger.info("information log")
// logger.error("error log")
logger.debug("debug log")

module.exports = APIError;


const developmentLogger = require('./request-logger')

let logger = null;

if (process.env.NODE_ENV === "development") {
    logger = developmentLogger()
}

module.exports = developmentLogger;
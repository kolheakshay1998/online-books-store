const { createLogger, format, transports } = require('winston');
const { combine, label, printf, json } = format;
let translateTime
const logger = createLogger({
  level: 'debuge',
  translateTime: 'IST:mm/dd/yyyy H:MM:ss T Z',

  transports: [
    // new (winston.transports.Http),
    new transports.File({
      name: 'online-bookstore-apis',
      prettifier: true,
      enabled: true,
      level: 'info',
      filename: `./logs/logs${new Date().toISOString().replace(/T.*/, '').split('-').reverse().join('-')}.txt`,
      mkdir: true,
      // handleExceptions: true,
      json: true,
      colorize: true,
      timestamp: true
      // translateTime: 'IST:mm/dd/yyyy H:MM:ss T Z'
    })
  ],
  // exitOnError: false
});
logger.stream = {
  write: function (message, encoding) {
    logger.info(message);
  }
};

process.on('unhandledRejection', (reason, p) => {
  logger.error('exception occur');
  throw reason;
});

module.exports = logger, this.stream, this.on





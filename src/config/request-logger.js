const winston = require('winston');
// const HttpLogger = require('winston-http');
const logger = require('./logger');

const ExpressLogger = () => HttpLogger({
  logger,
  serializers: {
    err: winston.stdSerializers.err,
    req: winston.stdSerializers.req,
    res: winston.stdSerializers.res,
    req(req) {
      req.body = req.raw.body
      req.params = req.raw.params
      req.query = req.raw.query
      return req;
    },
    // req(req) {
    //   req.params = req.raw.params;
    //   return req;
    // },
    // req(req) {
    //   req.query = req.raw.query;
    //   return req;
    // },
    // req(req) {
    //   req.custom  = {body:req['raw']['body'],params:req['raw']['params'],query:req['raw']['query']};
    //   return req.custom;
    // },
  },
  wrapSerializers: true,
});

module.exports = ExpressLogger
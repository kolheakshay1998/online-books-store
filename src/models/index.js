const fs = require('fs');
const { default: mongoose, Mongoose } = require('mongoose');
const path = require('path');
const { NODE_ENV } = require('../config/env-vars');

const config = require(path.join(process.cwd(), 'src', 'config', 'db-config.js'))[NODE_ENV];

const db = {};
const basename = path.basename(__filename);

fs.readdirSync(__dirname)
  .filter((file) => file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js')
  .forEach((file) => {
    const model = require(path.join(__dirname, file))(
    mongoose,
    Mongoose.DataTypes,
    );
    db[model.name] = model;
  });
Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

module.exports = db;


const mongoose = require('mongoose');

const transactionSchema = new mongoose.Schema({
    transactionId: String,
    guestUserSessionId: String,
    status: String,
    type: String,
    currencyIsoCode: String,
    amount: String,
    amountRequested: String,
    cardLastFourDigit: String,
    cardType: String,
    expirationMonth: String,
    expirationYear: String,
    paymentReceiptId: String,
    globalId: String,
    processorResponseCode: String,
    processorResponseText: String,
})

module.exports = mongoose.model("transaction", transactionSchema);
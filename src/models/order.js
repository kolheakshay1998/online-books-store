const mongoose = require('mongoose');

const cartEntrySchema = new mongoose.Schema({
  bookId: String,
  bookName: String,
  CoverImage: String,
  description: String,
  bookPrice: Number,
  quantity: Number,
  totalAmount: Number
});

const addressEntrySchema = new mongoose.Schema({
  flatno: String,
  address: String,
  landmark: String,
  city: String,
  state: String,
  pincode: Number,
  mobile: String,
});

const customerInfoSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  mobileNo: String,
  email: String,
});

const orderSchema = new mongoose.Schema({
  cartId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'cart'
  },
  paymentStatus: {
    type: String,
    default: ""
  },
  deliveryDate: {
    type: String
  },
  total: {
    type: Number,
    required: true
  },
  extraCharges: {
    type: Number,
    default: 0
  },
  discountAmount: {
    type: Number,
    default: 0
  },
  grandTotalAmount: {
    type: Number
  },
  paymentType: {
    type: String,
    enum: ["COD", "UPI", "CARD"],
    trim: true,
  },
  address: addressEntrySchema,
  customerInfo: customerInfoSchema,
  guestUserSessionId: {
    type: String,
  },
  cartEntries: [cartEntrySchema],

},
  {
    timestamps: true
  });

module.exports = mongoose.model("Order", orderSchema);
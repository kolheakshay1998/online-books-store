const mongoose = require('mongoose');

const bookRatingSchema = new mongoose.Schema({
    bookId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'book'
    },
    userName: {
        type: String,
        default: "GuestUser"
    },
    comments: String,
    rating: Number,
})

module.exports = mongoose.model("rating", bookRatingSchema);
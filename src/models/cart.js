const mongoose = require('mongoose');
 
const cartSchema = mongoose.Schema({
    guestUserSessionId: { type: String },
  cartEntries: [
    {
      bookId: { type: String },
      bookName: { type: String },
      CoverImage: { type: String },
      description: { type: String },
      bookPrice: { type: Number },
      quantity: { type: Number },
    },
  ],
},
{ versionKey: false },
);
module.exports = mongoose.model("cart", cartSchema);
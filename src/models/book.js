const mongoose = require('mongoose');
const bookSchema = mongoose.Schema({
    BookName: {
        type: String,
        required: true,
    },
    ISBN: {
        type: String,
        default: "",
        unique: true
    },
    SKU: {
        type: String,
    },
    Binding: {
        type: String,
        enum: ["paperback", "looseleaf", "hardcover", "bundle", "boardbook", "kindle ebooks", "audible audio edition"],
        trim: true,
        // required: true,
    },
    AutherName: {
        type: String,
        required: true,
    },
    AutherID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'auther',
    },
    PublisherName: {
        type: String,
        required: false,
    },
    PublisherID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'publisher',
    },
    PublishDate:
    {
        type: Date,
        required: true,
    },
    BookType: {
        type: [String],
        default: [],
    },
    Description: {
        type: String,
        required: true,
    },
    Language: {
        type: String,
        required: false,
    },
    Number_of_Pages: {
        type: Number,
        required: false,
    },
    Avalability: {
        type: Boolean,
        default: true
    },
    BarcodeId: {
        type: String,
        required: false,
    },
    OriginalBookPrice: {
        type: Number,
        required: true,
    },
    SellingBookPrice: {
        type: Number,
        required: true,
    },
    CoverImage: {
        type: [String],
        default: [],
    },
    Edition: {
        type: Number,
        required: false                 //optional 
    },
    SellerInformation: {
        type: String,
        required: false,
    },
},
    {
        timestamps: true,
    }
);
module.exports = mongoose.model("book", bookSchema);
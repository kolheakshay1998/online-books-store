const { Router } = require('express');

const router = Router();

router.use('/books', require('./book/books.routes'));
router.use('/cart', require('./cart/cart.routes'));
router.use('/order', require('./order/order.routes'));
router.use('/payment', require('./payment/payment.routes'));

module.exports = router;

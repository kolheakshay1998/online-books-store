const { Router } = require('express');
const validate = require('../../core/validate');

const bookController = require('./books.controller');
const bookValidation = require('./books.validation');

const router = Router();

router.post('/createBook', validate(bookValidation.CreateBook), bookController.CreateBook);
router.post('/booksRating', validate(bookValidation.BooksRating), bookController.BooksRating);
router.get('/listofBooks', bookController.ListOfBook);
router.get('/bookDetail/:id', validate(bookValidation.BookDetail), bookController.BookDetail);
router.put('/updateBook/:_id', validate(bookValidation.UpdateBook), bookController.UpdateBook);
router.get('/:searchField', validate(bookValidation.SearchBook), bookController.SearchBook)
router.delete('/removeBook/:bookId', validate(bookValidation.RemoveBook), bookController.RemoveBook);

module.exports = router;
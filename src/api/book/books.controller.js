const db = require('../../models/book');
const { BOOK_FIELDS, SEARCH_FIELDS, allowedFields, requiredFields, performanceHeavyTask } = require('../constants');
const { successResponse, errorResponse } = require('../../helpers/response');
const BooksRatingSchema = require('./books.validation').BooksRating
const multer = require('multer');
const path = require('path');
const logger = require('../../config/logger');
const bookrating = require('../../models/bookRating');
const { ObjectId } = require('mongodb');
const fs = require('fs').promises;

// create book with multiple images 
const CreateBook = async (req, res, next) => {
  try {
    uploadMultipleImages(req, res, async (err) => {
      if (err) {
        return res.status(400).json({ success: 0, message: 'File upload failed.....' });
      }

      // Map uploaded images only after successful file uploads
      const uploadedImages = req.files.map((file) => {
        return `${file.filename}`;
      });

      // Check for extra fields
      const extraFields = Object.keys(req.body).filter(field => !allowedFields.includes(field));
      if (extraFields.length > 0) {
        return res.status(400).json({ success: 0, message: `Extra fields not allowed : ${extraFields.join(', ')}` });
      }

      // Check for missing required fields
      const missingFields = requiredFields.filter(field => !req.body[field]);
      if (missingFields.length > 0) {
        return res.status(400).json({ success: 0, message: `Missing required fields: ${missingFields.join(', ')}` });
      }

      // Check if a book with the same data already exists
      const existingBook = await db.findOne({
        BookName: req.body.BookName,
        BarcodeId: req.body.BarcodeId,
        ISBN: req.body.ISBN,
        // Description: req.body.Description,
        AutherName: req.body.AutherName,
        // SellingBookPrice: req.body.SellingBookPrice
      });

      if (existingBook) {
        return res.status(400).json({ success: 0, message: 'A book with the same data already exists....' });
      }

      const bookData = await db.create({
        BookName: req.body.BookName,
        ISBN: req.body.ISBN,
        SKU: req.body.SKU,
        BarcodeId: req.body.BarcodeId,
        Binding: req.body.Binding,
        CoverImage: uploadedImages,
        Description: req.body.Description,
        AutherName: req.body.AutherName,
        PublisherName: req.body.PublisherName,
        PublishDate: req.body.PublishDate,
        BookType: req.body.BookType,
        Language: req.body.Language,
        Number_of_Pages: req.body.Number_of_Pages,
        Avalability: req.body.Avalability,
        OriginalBookPrice: req.body.OriginalBookPrice,
        SellingBookPrice: req.body.SellingBookPrice,
        Edition: req.body.Edition,
        SellerInformation: req.body.SellerInformation
      });

      return successResponse(res, bookData);
    });
  } catch (err) {
    next(err);
    logger.error(err);
  }
};

const storage = multer.diskStorage({
  destination: './upload/bookimages',
  filename: (req, file, cb) => {
    return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
  }
});

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 10 * 1024 * 1024, // 10 MB (in bytes)
    files: 5
  }
});

// const uploadSingleImage = upload.array('bookimage', 50);
const uploadMultipleImages = upload.array('CoverImage', 5);
const uploadImage = (req, res) => {
  if (req.file) {
    res.json({
      success: 1,
      profile_url: `${req.file.filename}`
    });
  } else {
    res.json({
      success: 0,
      message: 'File upload failed....'
    });
  }
};

//add books review with ratings and comment
const BooksRating = async (req, res, next) => {
  try {
    const {bookId, userName, comments, rating} = req.body

    const { error } = BooksRatingSchema.validate({ bookId, userName, comments, rating }, { abortEarly: false });
    if (error) {
      return res.status(400).json({ error: error.details.map((detail) => detail.message) });
    }

    const bookInfo = await db.findById({ _id: bookId }).select({ "__v": 0 })
    if (!bookInfo) {
      return res.status(404).json({ message: 'Book not found...' });
    }
    if (rating <= 5) {
      const booksReviews = await bookrating.create({
        bookId, userName, comments, rating
      });
      return successResponse(res, booksReviews);
    } else {
      return errorResponse(res, "please provide rating between 1 to 5")
    }
  } catch (err) {
    next(err)
    logger.error(err);
  }
};

//get all the list of all books or list of books as per avalability with response based on fields passed
const ListOfBook = async (req, res, next) => {
  try {
    performanceHeavyTask();

    const page = req.query.page || 1;
    const pageSize = req.query.pageSize || 100;

    const skip = (page - 1) * pageSize;

    let query = {};

    if (req.query.Avalability !== undefined) {
      const Avalability = req.query.Avalability === 'true';
      query = { Avalability: Avalability };
    }

    const requestedFields = req.query.fields ? req.query.fields.split(',') : BOOK_FIELDS;

    const invalidFields = requestedFields.filter(field => !BOOK_FIELDS.includes(field));
    if (invalidFields.length > 0) {
      return res.status(400).json({ error: `Invalid fields: ${invalidFields.join(', ')}` });
    }

    const list = await db.find(query).select(requestedFields.join(' ')).skip(skip).limit(pageSize);

    return successResponse(res, list);
  } catch (err) {
    next(err);
    logger.error(err);
  }
};

//get complete detail of selected book 
const BookDetail = async (req, res, next) => {
  try {
    const bookId = req.params.id
    const bookInfo = await db.findById({ _id: bookId }).select({ "__v": 0 })
    const data = await bookrating.find({ bookId }).select({ "__v": 0 })
    if (!bookInfo) {
      return res.status(404).json({ message: 'Book not found...' });
    }

    const ratingStats = await bookrating.aggregate([
      { $match: { bookId: ObjectId(bookId) } },
      {
        $group: {
          _id: null,
          averageRating: { $avg: "$rating" }
        }
      }
    ]);
    const overAllRating = parseFloat((ratingStats.length > 0 ? ratingStats[0].averageRating : 0).toFixed(1));

    const booksReviews = data.map(({ userName, comments, rating }) => ({
      userName,
      comments,
      rating
    }));

    return successResponse(res, { bookInfo, overAllRating, booksReviews });
  } catch (err) {
    next(err);
    logger.error(err);
  }
};

//update the book with require fields with images
const UpdateBook = async (req, res, next) => {
  try {
    uploadMultipleImages(req, res, async (err) => {
      if (err) {
        return res.status(400).json({ success: 0, message: 'File upload failed.....' });
      }

      const uploadedImages = req.files.map((file) => {
        return `${file.filename}`;
      });

      // Check if the book with the given ID exists
      const existingBook = await db.findOne({ _id: req.params._id });

      if (!existingBook) {
        return res.status(404).json({ success: 0, message: 'Book not found....' });
      }

      // Check for extra fields in the request body
      const extraFields = Object.keys(req.body).filter(field => !allowedFields.includes(field));
      if (extraFields.length > 0) {
        return res.status(400).json({ success: 0, message: `Extra fields not allowed: ${extraFields.join(', ')}` });
      }

      for (const key in req.body) {
        if (allowedFields.includes(key) && key !== 'CoverImage') {
          existingBook[key] = req.body[key];
        }
      }

      if (uploadedImages.length > 0) {
        existingBook.CoverImage = uploadedImages;
      }

      const updatedBook = await existingBook.save();

      return successResponse(res, updatedBook);
    });
  } catch (err) {
    next(err);
    logger.error(err);
  }
};

// search book with one input field but with multiple fields option 
const SearchBook = async (req, res, next) => {
  try {
    performanceHeavyTask();

    const searchField = req.params.searchField;

    const query = {
      $or: SEARCH_FIELDS.reduce((data, field) => {
        data.push({ [field]: { $regex: searchField, $options: 'i' } });
        return data;
      }, [])
    };

    const FoundBooks = await db.find(query);

    if (FoundBooks.length === 0) {
      return errorResponse(res, 'No such book is currently available.....');
    }

    return successResponse(res, FoundBooks);
  } catch (err) {
    next(err);
    logger.error(err);
  }
};

// remove/delete the specific book along with images
const RemoveBook = async (req, res, next) => {
  try {
    const bookId = req.params.bookId
    const book = await db.findById(bookId);

    if (!book) {
      return res.status(404).json({ success: 0, message: 'Book not found....' });
    }

    const imageFilenames = book.CoverImage;
    for (const filename of imageFilenames) {
      await fs.unlink(`./upload/bookimages/${filename}`);
    }

    await db.findByIdAndDelete(bookId);

    return successResponse(res, { message: 'Book deleted successfully' });
  } catch (err) {
    next(err);
    logger.error(err);
  }
};

module.exports = {
  CreateBook,
  ListOfBook,
  BooksRating,
  BookDetail,
  UpdateBook,
  SearchBook,
  RemoveBook,
};

const Joi = require('joi');
// const { join } = require('lodash');
// const JoiObjectId = require("joi-objectid");
// const myJoiObjectId = JoiObjectId(Joi);

const CreateBook = {
      FormData: {
        BookName: Joi.string().required(), 
        ISBN: Joi.string().optional(), 
        SKU: Joi.string().optional(), 
        BarcodeId: Joi.string().optional(), 
        Binding: Joi.string().optional(), 
        CoverImage: Joi.string().optional(), 
        Description: Joi.string().required(), 
        AutherName: Joi.string().required(), 
        PublisherName: Joi.string().optional(), 
        PublishDate: Joi.date().optional(), 
        BookType: Joi.string().optional(), 
        Language: Joi.string().optional(),
        Number_of_Pages: Joi.number().optional(), 
        OriginalBookPrice: Joi.number().optional(), 
        SellingBookPrice: Joi.number().optional(), 
        Edition: Joi.string().optional(), 
        SellerInformation: Joi.string().optional()
      }
  };

const BooksRating = Joi.object({
  bookId: Joi.string().required(),
  userName: Joi.string().optional(),
  comments: Joi.string().optional(),
  rating: Joi.number().required()
});

  const UpdateBook = {
    FormData: {
      BookName: Joi.string().optional(), 
      ISBN: Joi.string().optional(), 
      SKU: Joi.string().optional(), 
      BarcodeId: Joi.string().optional(), 
      Binding: Joi.string().optional(), 
      CoverImage: Joi.string().optional(), 
      Description: Joi.string().optional(), 
      AutherName: Joi.string().optional(), 
      PublisherName: Joi.string().optional(), 
      PublishDate: Joi.date().optional(), 
      BookType: Joi.string().optional(), 
      Language: Joi.string().optional(),
      Number_of_Pages: Joi.number().optional(), 
      OriginalBookPrice: Joi.number().optional(), 
      SellingBookPrice: Joi.number().optional(), 
      Edition: Joi.string().optional(), 
      SellerInformation: Joi.string().optional()
    }
};

  const BookDetail ={
    params: {
      id: Joi.string().required(),
  }
  };

  const SearchBook ={
    params: {
      searchField: Joi.string().required(),
  }
  };

  const RemoveBook ={
    params: {
      bookId: Joi.string().required(),
  }
  };
  
  module.exports = {
    CreateBook,
    BooksRating,
    BookDetail,
    SearchBook,
    RemoveBook,
    UpdateBook
  }
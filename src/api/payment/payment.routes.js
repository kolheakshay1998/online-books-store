const express = require('express');
const paymentController = require('./payment.controller'); 

const router = express.Router();

router.post('/generateToken', paymentController.generateToken);
router.post('/processPayment', paymentController.processPayment);

module.exports = router;

require('dotenv').config()
const braintree = require('braintree');
const { successResponse, errorResponse } = require('../../helpers/response');

var gateway = new braintree.BraintreeGateway({
    environment: process.env.BRAINTREE_ENVIRONMENT || braintree.Environment.Sandbox,
    merchantId: process.env.BRAINTREE_MERCHANT_ID,
    publicKey: process.env.BRAINTREE_PUBLIC_KEY,
    privateKey: process.env.BRAINTREE_PRIVATE_KEY
});

//generate client token
const generateToken = (req, res, next) => {
    gateway.clientToken.generate({}).then((response) => {
        res.status(200).send(response)
    }).catch(err => res.status(500).send(err))
}

//payment transaction process 
const processPayment = async (req, res, next) => {
    try {
        const nonceFromTheClient = req.body.payment_method_nonce;
        const amount = req.body.amount;

        const data = await gateway.transaction.sale({
            amount: amount,
            paymentMethodNonce: nonceFromTheClient,
            options: {
                submitForSettlement: true
            }
        })
        return successResponse(res, data);
    } catch (err) {
        next(err)
    }
};


module.exports = {
    generateToken,
    processPayment
};
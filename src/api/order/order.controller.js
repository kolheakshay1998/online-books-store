const logger = require('../../config/logger');
require('dotenv').config();
const db = require('../../models/order');
const CART = require('../../models/cart');
const trans = require('../../models/transaction');
const CreateOrderSchema = require('./order.validation').CreateOrder
const braintree = require('braintree');
const { successResponse, errorResponse } = require('../../helpers/response');

var gateway = new braintree.BraintreeGateway({
  environment: process.env.BRAINTREE_ENVIRONMENT || braintree.Environment.Sandbox,
  merchantId: process.env.BRAINTREE_MERCHANT_ID,
  publicKey: process.env.BRAINTREE_PUBLIC_KEY,
  privateKey: process.env.BRAINTREE_PRIVATE_KEY
});

// create order for added books with payment details
const CreateOrder = async (req, res, next) => {
  try {
    let { guestUserSessionId, cartId } = req.body;
    let query = {};
    const address = req.body.address;
    const customerInfo = req.body.customerInfo;

    const { error } = CreateOrderSchema.validate(req.body, { abortEarly: false });
 
    if (error) {
      return res.status(400).json({ error: error.details.map((detail) => detail.message) });
    }

    if (guestUserSessionId) {
      query = { guestUserSessionId };
    } else if (cartId) {
      query = { _id: cartId };
    }

    const cartBooks = await CART.findOne(query);

    if (!cartBooks || cartBooks.cartEntries.length === 0) {
      return res.status(404).json({ error: 'Cart is empty.' });
    }

    const orderTotal = calculateTotal(cartBooks);

    const cartEntriesForBraintree = cartBooks.cartEntries.map((book, index) => ({
      kind: 'debit',
      totalAmount: (book.bookPrice * book.quantity).toFixed(2),
      name: book.bookName,
      description: book.description,
      quantity: book.quantity,
      unitAmount: book.bookPrice.toFixed(2),
    }));
    const cartEntries = cartBooks.cartEntries.map((book, index) => ({
      totalAmount: (book.bookPrice * book.quantity).toFixed(2),
      bookId: book.bookId,
      // CoverImage: book.CoverImage,
      bookName: book.bookName,
      // description: book.description,
      quantity: book.quantity,
      bookPrice: book.bookPrice.toFixed(2),
    }));

    // Generate a client token for the client-side
    const clientToken = await gateway.clientToken.generate({});

    const fakePaymentMethodNonce = 'fake-valid-nonce';
    // Process payment using Braintree
    // const nonceFromTheClient = req.body.payment_method_nonce;

    const braintreeResult = await gateway.transaction.sale({
      amount: orderTotal,
      paymentMethodNonce: fakePaymentMethodNonce,
      options: {
        submitForSettlement: true,
      },
      lineItems: cartEntriesForBraintree,
    });


    if (!braintreeResult.success) {
      logger.error('Payment processing failed.', { details: braintreeResult.errors })
      return res.status(500).json({ error: 'Payment processing failed.', details: braintreeResult.errors });
    }

    const sessionId = cartBooks.guestUserSessionId;

    const orderData = {
      paymentStatus: "success",
      total: orderTotal,
      extraCharges: req.body.extraCharges || 0,
      paymentType: req.body.paymentType,
      address: { ...address },
      customerInfo: { ...customerInfo },
      grandTotalAmount: orderTotal + (req.body.extraCharges || 0),
      guestUserSessionId: sessionId,
      cartEntries: cartEntries,
    };

    const order = await db.create(orderData);

    const transaction = [];
    transaction.push({
      transactionId: braintreeResult.transaction.id,
      guestUserSessionId: sessionId,
      status: braintreeResult.transaction.status,
      type: braintreeResult.transaction.type,
      currencyIsoCode: braintreeResult.transaction.currencyIsoCode,
      amount: braintreeResult.transaction.amount,
      amountRequested: braintreeResult.transaction.amountRequested,
      cardLastFourDigit: braintreeResult.transaction.creditCard.last4,
      cardType: braintreeResult.transaction.creditCard.cardType,
      expirationMonth: braintreeResult.transaction.creditCard.expirationMonth,
      expirationYear: braintreeResult.transaction.creditCard.expirationYear,
      paymentReceiptId: braintreeResult.transaction.paymentReceipt.id,
      globalId: braintreeResult.transaction.paymentReceipt.globalId,
      processorResponseCode: braintreeResult.transaction.paymentReceipt.processorResponseCode,
      processorResponseText: braintreeResult.transaction.paymentReceipt.processorResponseText,
    })
    const transactionLog = await trans.insertMany(transaction)

    // Clear the cart 
    // await CART.deleteMany({ query });

    // Void the transaction 
    const voidResult = await gateway.transaction.void(braintreeResult.transaction.id);
    if (!voidResult.success) {
      logger.error('Transaction void failed.', { details: voidResult.errors });
      return res.status(500).json({ error: 'Transaction void failed.', details: voidResult.errors });
    }

    return successResponse(res, { order, transactionLog });
  } catch (err) {
    next(err);
    logger.error(err);
  }
};

// function to calculate the total order amount
function calculateTotal(cartBooks) {
  let total = 0;

  const flattenedEntries = cartBooks.cartEntries.flatMap(entry => entry);
  for (const book of flattenedEntries) {
    total += book.bookPrice * book.quantity;
  }
  return total;
};

//get the order details
const GetOrder = async (req, res, next) => {
  try {
    const { guestUserSessionId, orderId, mobileNo, email } = req.query;

    const filter = {};
    if (guestUserSessionId) {
      filter.guestUserSessionId = guestUserSessionId;
    }
    if (orderId) {
      filter._id = orderId;
    }
    if (mobileNo) {
      filter['customerInfo.mobileNo'] = mobileNo;
    }
    if (email) {
      filter['customerInfo.email'] = email;
    }
    const orders = await db.find(filter);

    if (orders.length === 0) {
      return res.status(404).json({ error: 'No orders found....' });
    }
    return successResponse(res, orders);
  } catch (err) {
    next(err);
    logger.error(err);
  }
};

module.exports = {
  CreateOrder,
  GetOrder
};

const Joi = require('joi');
// const { join } = require('lodash');
// const JoiObjectId = require("joi-objectid");
// const myJoiObjectId = JoiObjectId(Joi);

const CreateOrder= Joi.object({
  guestUserSessionId: Joi.string().optional(),
  cartId: Joi.string().optional(),
  address: Joi.object({
    address: Joi.string().required(),
    flatno: Joi.string().optional(),
    landmark :Joi.string().optional(),
    city: Joi.string().required(),
    state: Joi.string().required(),
    mobile: Joi.string().required(),
    pincode: Joi.number().required()
  }).required(),
  customerInfo: Joi.object({
    lastName : Joi.string().required(),
    firstName: Joi.string().required(),
    mobileNo: Joi.string().required(),
    email: Joi.string().email().optional(),
  }).required(),
  paymentType: Joi.string().optional(),
  extraCharges: Joi.number().optional(),
  payment_method_nonce: Joi.string().optional(), 
});

const GetOrder = {
  query: {
    guestUserSessionId: Joi.string().optional(),
    orderId: Joi.string().optional(),
    mobileNo: Joi.string().optional(),
    email: Joi.string().optional(),
  }
};
module.exports = {
  CreateOrder,
  GetOrder
}
const { Router } = require('express');
const validate = require('../../core/validate');

const orderController = require('./order.controller');
const orderValidation = require('./order.validation');

const router = Router();

router.post('/createorder', validate(orderValidation.CreateOrder), orderController.CreateOrder);
router.get('/getorder', validate(orderValidation.GetOrder), orderController.GetOrder);

module.exports = router;
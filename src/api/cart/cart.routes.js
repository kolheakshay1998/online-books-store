const { Router } = require('express');
const validate = require('../../core/validate');

const cartController = require('./cart.controller');
const cartValidation = require('./cart.validation');

const router = Router();

router.post('/createCart', cartController.CreateCart);
router.get('/viewCart', validate(cartValidation.ViewCart), cartController.ViewCart);
router.put('/incQuantity', validate(cartValidation.IncQuantity), cartController.IncQuantity);
router.put('/decQuantity', validate(cartValidation.DecQuantity), cartController.DecQuantity);
router.delete('/removeBook', validate(cartValidation.RemoveBook), cartController.RemoveBook);
router.delete('/deleteCart', validate(cartValidation.DeleteCart), cartController.DeleteCart);

module.exports = router;
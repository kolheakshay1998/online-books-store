const Joi = require('joi');

const CreateCart = Joi.object({
  cart: Joi.array().items({
    bookId: Joi.string().required(),
    bookName: Joi.string().required(),
    description: Joi.string().optional(),
    bookPrice: Joi.number().required(),
    quantity: Joi.number().required(),
  }).required(),
      guestUserSessionId: Joi.string().optional(),
  });

  const ViewCart = {
    params: {
      guestUserSessionId: Joi.string().optional(), 
      cartId: Joi.string().optional()
    }
  };

  const IncQuantity = Joi.object({
      guestUserSessionId: Joi.string().optional(),
      cartId: Joi.string().optional(), 
      bookId: Joi.string().required(), 
      quantity: Joi.number().optional(), 
  });

  const DecQuantity = Joi.object({
      guestUserSessionId: Joi.string().optional(),
      cartId: Joi.string().optional(), 
      bookId: Joi.string().required(),
      quantity: Joi.number().optional(),   
  });

  const RemoveBook = Joi.object({
      guestUserSessionId: Joi.string().optional(),
      cartId: Joi.string().optional(), 
      bookId: Joi.string().required(),  
  });

  const DeleteCart = {
    query: {
      guestUserSessionId: Joi.string().optional(),
      cartId: Joi.string().optional(), 
    },
  };

  module.exports = {
    CreateCart,
    ViewCart,
    IncQuantity,
    DecQuantity,
    RemoveBook,
    DeleteCart
  }
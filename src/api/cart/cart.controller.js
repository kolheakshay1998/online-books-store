const db = require('../../models/cart');
const { successResponse, errorResponse } = require('../../helpers/response');
const BOOK = require('../../models/book');
const createCartSchema = require('./cart.validation').CreateCart
const IncQuantitySchema = require('./cart.validation').IncQuantity
const DecQuantitySchema = require('./cart.validation').DecQuantity
const RemoveBookSchema = require('./cart.validation').RemoveBook
const logger = require('../../config/logger');
const crypto = require('crypto');
//create cart API if cart is present update the cart
const CreateCart = async (req, res, next) => {
  try {
    let { guestUserSessionId } = req.query;
    const cart = req.body.cart;
    const { error } = createCartSchema.validate(req.body,{ abortEarly: false });
 
    if (error) {
      // Return validation error response
      return res.status(400).json({ error: error.details.map((detail) => detail.message) });
    }
    const existingCart = await db.findOne({ guestUserSessionId });

    if (!existingCart) {
      // If the cart doesn't exist, create a new one
      function generateRandomString(length) {
        return crypto.randomBytes(Math.ceil(length / 2)).toString('hex').slice(0, length);
      }

      const randomString = generateRandomString(10);

      let guestUserSessionId = Math.floor((Math.random() * 9000000000000000));
      guestUserSessionId = randomString + guestUserSessionId;
      const newCartEntries = await Promise.all(cart.map(async (book) => {
        const CoverImage = await BOOK.findById(book.bookId);

        return {
          bookId: book.bookId,
          bookName: book.bookName,
          CoverImage: CoverImage.CoverImage[0],
          description: book.description,
          bookPrice: book.bookPrice,
          quantity: book.quantity,
        };
      }));

      const myCart = await db.create({ guestUserSessionId, cartEntries: newCartEntries });

      const cartData = {
        _id: myCart._id,
        cartEntries: myCart.cartEntries,
        __v: 0,
      };

      return successResponse(res, { guestUserSessionId, cartData });
    } else {
      // If the cart already exists, update it with new entries
      existingCart.cartEntries = existingCart.cartEntries || [];

      const newCartEntries = await Promise.all(cart.map(async (book) => {
        const CoverImage = await BOOK.findById(book.bookId);

        return {
          bookId: book.bookId,
          bookName: book.bookName,
          CoverImage: CoverImage.CoverImage[0],
          description: book.description,
          bookPrice: book.bookPrice,
          quantity: book.quantity,
        };
      }));

      existingCart.cartEntries.push(...newCartEntries);
      await existingCart.save();

      const cartData = {
        _id: existingCart._id,
        cartEntries: existingCart.cartEntries,
        __v: 0,
      };

      return successResponse(res, { guestUserSessionId, cartData });
    }
  } catch (err) {
    next(err);
    logger.error(err);
  }
};

//view the cart details with guestUserSessionId, cartId
const ViewCart = async (req, res, next) => {
  try {
    let { guestUserSessionId, cartId } = req.body;
    let query = {};
    if (guestUserSessionId) {
      query = { guestUserSessionId };
    } else if (cartId) {
      query = { _id: cartId };
    }

    if (Object.keys(query).length === 0) {
      return errorResponse(res, "please send the guestUserSessionId or cartId");
    }
    // if (guestUserSessionId === undefined & cartId === undefined)
    // {
    //   return res.status(400).json({message: 'guestUserSessionId & cartId is missing...'});
    // }
    const cart = await db.findOne(query);

    if (!cart) {
      return successResponse(res, { guestUserSessionId, cartData: "cart is Empty" });
    }
    const cartEntries = cart.cartEntries.map((entry) => ({
      bookId: entry.bookId,
      bookName: entry.bookName,
      description: entry.description,
      bookPrice: entry.bookPrice,
      quantity: entry.quantity,
    }));

    // Calculate totalAmount
    const totalAmount = cartEntries.reduce((total, entry) => total + entry.bookPrice * entry.quantity, 0);

    const cartData = {
      _id: cart._id,
      cartEntries,
      totalAmount,
      __v: cart.__v,
    };

    guestUserSessionId = cart.guestUserSessionId
    return successResponse(res, { guestUserSessionId, cartData });
  } catch (err) {
    next(err);
    logger.error(err);
  }
};

// update specific books quantity by incrementing 
const IncQuantity = async (req, res, next) => {
  try {
    let { guestUserSessionId, cartId } = req.query;
    let query = {};
    const { bookId, quantity } = req.body;

    if (guestUserSessionId) {
      query = { guestUserSessionId };
    } else if (cartId) {
      query = { _id: cartId };
    }

    if (Object.keys(query).length === 0) {
      return errorResponse(res, "please send the guestUserSessionId or cartId");
    }
    const combinedData = { ...req.body, ...req.query };
 
    const { error } = IncQuantitySchema.validate(combinedData, { abortEarly: false });
 
    if (error) {
      return res.status(400).json({ error: error.details.map((detail) => detail.message) });
    }
    const incqty = await db.findOneAndUpdate(
      { ...query, "cartEntries.bookId": bookId },
      { $inc: { "cartEntries.$.quantity": quantity || 1 } },
      { new: true }
    );

    return successResponse(res, incqty);
  } catch (err) {
    next(err);
    logger.error(err);
  }
};

// update specific books quantity by decrementing 
const DecQuantity = async (req, res, next) => {
  try {
    let { guestUserSessionId, cartId } = req.query;
    let query = {};
    const { bookId, quantity } = req.body;

    if (guestUserSessionId) {
      query = { guestUserSessionId };
    } else if (cartId) {
      query = { _id: cartId };
    }

    if (Object.keys(query).length === 0) {
      return errorResponse(res, "please send the guestUserSessionId or cartId");
    }
    const combinedData = { ...req.body, ...req.query };
 
    const { error } = DecQuantitySchema.validate(combinedData, { abortEarly: false });
 
    if (error) {
      // Return validation error response
      return res.status(400).json({ error: error.details.map((detail) => detail.message) });
    }
    const decqty = await db.findOneAndUpdate(
      { ...query, "cartEntries.bookId": bookId },
      { $inc: { "cartEntries.$.quantity": -(quantity || 1) } },
      { new: true }
    );
    return successResponse(res, decqty);
  } catch (err) {
    next(err)
  }
};

//remove specific selected book from cart 
const RemoveBook = async (req, res, next) => {
  try {
    let { guestUserSessionId, cartId } = req.query;
    let query = {};
    const { bookId } = req.body;

    if (guestUserSessionId) {
      query = { guestUserSessionId };
    } else if (cartId) {
      query = { _id: cartId };
    }

    if (Object.keys(query).length === 0) {
      return errorResponse(res, "please send the guestUserSessionId or cartId");
    }
    const combinedData = { ...req.body, ...req.query };
 
    const { error } = RemoveBookSchema.validate(combinedData, { abortEarly: false });
 
    if (error) {
      return res.status(400).json({ error: error.details.map((detail) => detail.message) });
    }

    const existingCart = await db.findOne(query);

    if (!existingCart) {
      return errorResponse(res, "Cart is empty....");
    }

    const bookIndex = existingCart.cartEntries.findIndex(entry => entry.bookId === bookId);

    if (bookIndex !== -1) {
      // Remove the book from cartEntries array
      existingCart.cartEntries.splice(bookIndex, 1);

      await existingCart.save();

      const cartData = {
        _id: existingCart._id,
        cartEntries: existingCart.cartEntries,
        __v: 0,
      };

      return successResponse(res, { guestUserSessionId, cartData });
    } else {
      return errorResponse(res, "Book not found in the cart");
    }
  } catch (err) {
    next(err);
    logger.error(err);
  }
};

//empty/delete the cart
const DeleteCart = async (req, res, next) => {
  try {
    let { guestUserSessionId, cartId } = req.query;
    let query = {};

    if (guestUserSessionId) {
      query = { guestUserSessionId };
    } else if (cartId) {
      query = { _id: cartId };
    }

    if (Object.keys(query).length === 0) {
      return errorResponse(res, "please send the guestUserSessionId or cartId");
    }
    // if (guestUserSessionId === undefined & cartId === undefined)
    // {
    //   return res.status(400).json({message: 'guestUserSessionId & cartId is missing...'});
    // }
    const deletedCart = await db.deleteOne(query);

    if (deletedCart.deletedCount > 0) {
      return successResponse(res, { message: 'Cart deleted successfully' });
    } else {
      return errorResponse(res, 'Cart is empty....');
    }
  } catch (err) {
    next(err);
    logger.error(err);
  }
};


module.exports = {
  CreateCart,
  ViewCart,
  IncQuantity,
  DecQuantity,
  RemoveBook,
  DeleteCart
};

const allowedFields = ['BookName', 'ISBN', 'SKU', 'BarcodeId', 'Binding', 'CoverImage', 'Description', 'AutherName', 'PublisherName', 'PublishDate',
'BookType', 'Language', 'Number_of_Pages', 'Avalability', 'OriginalBookPrice', 'SellingBookPrice', 'Edition', 'SellerInformation'];

const requiredFields = ['BookName', 'BarcodeId', 'Description', 'AutherName', 'SellingBookPrice'];

const BOOK_FIELDS = [
    '_id',
    'BookName',
    'ISBN',
    'SKU',
    'Binding',
    'AutherName',
    'PublisherName',
    'PublishDate',
    'BookType',
    'Description',
    'Language',
    'Number_of_Pages',
    'BarcodeId',
    'OriginalBookPrice',
    'SellingBookPrice',
    'CoverImage',
    'Edition',
    'SellerInformation',
    'createdAt',
    'updatedAt',
    'Avalability',
  ];

  const SEARCH_FIELDS = [
    'BookName',
    'AutherName',
    'BookType',
    'Binding',
    'Language',
    'BarcodeId',
    'ISBN',
  ];
  
//function for getting heavy tasks (cpu profile)
  let heavyTaskMap = []
const performanceHeavyTask = () => {
  heavyTaskMap = [];
  for (let x=0; x<100000; ++x){
    heavyTaskMap.push(new Map())
  }
}
  module.exports = {
    BOOK_FIELDS, SEARCH_FIELDS, allowedFields, requiredFields, performanceHeavyTask
  };
  